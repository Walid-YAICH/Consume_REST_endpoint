package tn.esprit;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

/**
 * @author Walid-YAICH
 * 
 * URL utiles :
 * http://theoryapp.com/parse-json-in-java/
 * https://jsonformatter.curiousconcept.com/
 * https://github.com/Esprit-JavaEE/yahoofinance.git
 * 
 *
 */

public class MyIP {

	public static void main(String[] args) {
		String endpoint = "https://api.ipify.org?format=json";
		HttpClient client = new DefaultHttpClient();
		HttpGet request = new HttpGet(endpoint);
		try {
			HttpResponse response = client.execute(request);
			String jsonResponse = EntityUtils.toString(response.getEntity());
			System.out.println("Response as String : " + jsonResponse);
			JSONObject responseObj = new JSONObject(jsonResponse);
								
			System.out.println("/*****************************************/");
			System.out.println("Server public IP ADDRESS : " + responseObj.getString("ip"));
			System.out.println("/*****************************************/");
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
